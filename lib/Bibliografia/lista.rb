module Bibliografia
Node = Struct.new(:value, :next, :prev)

class Lista
	include Enumerable
	attr_reader :head
	def initialize()
		@head
	end

	def pop()
		if(@head != nil)
			aux = @head
			@head = @head.next
			return aux.value
		else
			return nil
		end
	end

	def push(value)
		if(@head != nil)
			aux = @head
			while (aux.next != nil)
				aux = aux.next
			end
			aux.next = Node.new(value, nil, aux)
		else
			@head = Node.new(value, nil, nil)
		end
	end

	def mulpush(valores)
		valores.each do |x|
			push(x)
		end
	end

	def to_s
		cadena = "#{@head.value.to_s}"
		aux = @head
		while(aux.next != nil)
			aux = aux.net
			cadena += " "
			cadena += "#{aux.value.to_s}"
		end
		cadena
	end
end

end
