require 'spec_helper'
require 'Bibliografia'

describe Bibliografia do

l1 = Bibliografia::Bibliografia.new('Dave Thomas, Andy Hunt, Chad Fowler', 'Programming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide (The Facets of Ruby)', 'Pragmatic Bookshelf', '4 edition', '','July 7 2013', '978-1937785499', '1937785491')
l2 = Bibliografia::Bibliografia.new('Scott Chacon', 'Pro Git 2009th Edition', '(Pro) Apress', '2009 edition', '', 'August 27, 2009', '978-
1430218333', '1430218339')
l3 = Bibliografia::Bibliografia.new('David Flanagan, Yukihiro Matsumoto','The Ruby Programming Language', 'O’Reilly Media', '1 edition', '', '(Fe-bruary 4, 2008)', '0596516177', '978-059651')
l4 = Bibliografia::Bibliografia.new('David Chelimsky, Dave Astels, Bryan Helmkamp, Dan North, Zach Dennis, Aslak Hellesoy', 'The RSpec Book: Behaviour Driven Development with RSpec, Cucumber, and Friends (The Facets of Ruby', 'Pragmatic Bookshelf', '', '1 edition' ,'December 25, 2010' ,'1934356379', '978-1934356371')
l5 = Bibliografia::Bibliografia.new('Richard E. Silverman', 'Git Pocket Guide','O’Reilly Media', '1 edition' , '', 'August 2, 2013', '1449325866', '978-1449325862')


	nodo = Bibliografia::Node.new(5,nil,nil)
	milista = Bibliografia::Lista.new()
	milista.push(l1)

	context "# PRUEBAS DEL NODO" do
		it "Debe existir un nodo de la lista con sus datos y su siguiente" do
		nodo = Bibliografia::Node.new(5,nil,nil)
	end
	end

	describe "# PRUEBAS DE LA LISTA" do
		it "Se extrae el primer elemento de la lista" do
			aux = milista.pop
			expect(aux.GetTitulo).to eq("Programming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide (The Facets of Ruby)")
		end
		it "Se puede insertar un elemento" do 
			milista.push(l2)				
		end
		it "Se puede insertar varios elementos" do 
			milista.push(l3)
			milista.push(l4)				
		end
	end
=begin		it "Se extrae el primer elemento de la lista" do
			aux = milista.pop
			expect(aux.GetTitulo).to eq("The Ruby Programming Language")
		end
=end
end
